var assert = require("chai").assert;
var expect = require('chai').expect;
var should = require('chai').should();

var utils = require('../../lib/utils.js');

describe('Running the basic util functions', function () {

// Create an account with some basic information...
    describe('basic functions', function () {
        it('should get a new response object.', function (done) {
            var response = utils.Response()
            assert.isObject(response);

            response.getHeaders();

            // all done with this test..
            done();
        });

        it('should get a new error object.', function (done) {
            var error = utils.Error()
            assert.isObject(error);

            // all done with this test..
            done();
        });

        it('should successfully handle the ifCondition function', function (done) {

            assert.equal(utils.ifCondition(1===1, 'true', 'false'), "true");
            assert.equal(utils.ifCondition(1!==1, 'true', 'false'), "false");

            // all done with this test..
            done();
        });


    });

    describe('during the paging interaction', function () {
        it('should get a new paging object.', function (done) {
            var response = utils.Paging()
            assert.isObject(response);

            // all done with this test..
            done();
        });



    });





});