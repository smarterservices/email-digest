var assert = require("chai").assert,
    expect = require('chai').expect,
    uuid = require('node-uuid'),
    uuid1 = uuid.v1(),
    uuid2 = uuid.v1(),
    clientid = uuid.v1(),
    config = require('../../config/'),
    batchid = "",
    random = (Math.random() * 10),
    number1 = parseInt(random, 10),
    number2 = parseInt(random, 10),
    number3 = parseInt(random, 10),
    number4 = parseInt(random, 10),
    number = (number1 + number2 + number3 + number4),
    digest = require("../../lib/digest")(config),
    group = require("../../lib/group")(config);


describe('Digest lib', function () {
    describe('should test addgroup function', function () {
        it('should fail based on email id', function (done) {
            var opts = {
                doc_type: 'group-subscription-config',
                group_config_id: "",
                application_id: "the-application-id",
                email_id: "thisiswrong",
                group_id: clientid,
                account_id: "uni",
                subject: "This is the subject",
                template: "this is the template",
                frequency: number + " 0-11 * * *",
                to: "address@gmail.com,secondaddress@gmail.com,jordan@gmail.com",
                bcc: "jpiepkow@gmail.com,jpiepkow@gmail.com,jpiepkow@gmail.com"
            };
            group.addGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid keys");
                expect(result).to.be.a('undefined');
                done();
            });

        });
        it('should fail based on application id', function (done) {
            var opts = {
                doc_type: 'group-subscription-config',
                group_config_id: "",
                application_id: "thisiswrong",
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                subject: "This is the subject",
                template: "this is the template",
                frequency: number + " 0-11 * * *",
                to: "address@gmail.com,secondaddress@gmail.com,jordan@gmail.com",
                bcc: "jpiepkow@gmail.com,jpiepkow@gmail.com,jpiepkow@gmail.com"
            };
            group.addGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid keys");
                expect(result).to.be.a('undefined');
                done();
            });

        });
        it('add new group', function (done) {
            var opts = {
                doc_type: 'group-subscription-config',
                group_config_id: "",
                application_id: "the-application-id",
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                template: "this is the template",
                frequency: number + " 0-11 * * *",
                to: "jpiepkow@gmail.com",
                bcc: "jpiepkow@gmail.com"
            };
            group.addGroup(opts, function (err, result) {
                expect(err).to.be.a('undefined');
                expect(result).to.be.a('object');
                done();
            });

        });
        it('add new group with a new account', function (done) {
            var opts = {
                doc_type: 'group-subscription-config',
                group_config_id: "",
                application_id: "the-application-id",
                email_id: "1234",
                group_id: clientid + "add",
                account_id: clientid,
                template: "this is the template",
                frequency: number + " 0-11 * * *",
                to: "jpiepkow@gmail.com",
                bcc: "jpiepkow@gmail.com"
            };
            group.addGroup(opts, function (err, result) {
                expect(err).to.be.a('undefined');
                expect(result).to.be.a('object');
                done();
            });

        });
        it('should fail based on dup group_id', function (done) {
            var opts = {
                doc_type: 'group-subscription-config',
                group_config_id: "",
                application_id: "the-application-id",
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                subject: "This is the subject",
                template: "this is the template",
                frequency: number + " 0-11 * * *",
                to: "jpiepkow@gmail.com",
                bcc: "jpiepkow@gmail.com"
            };
            group.addGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Client ID already exists");
                expect(result).to.be.a('undefined');
                done();
            });

        });
    });
    /**
     *
     * add new batch function here
     *
     *
     *
     */
    describe('Testing to add content making a new batch', function () {
        it('Should fail because email key is wrong', function (done) {
            var opts = {
                email_id: "thisiswrong",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content"
            };
            digest.addContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "No match on email type found");
                expect(result).to.be.a('null');
                done();
            });
        });
        it('Should create content in a new batch', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content"
            };
            digest.addContent(opts, function (err, result) {
                batchid = result.batch_id;
                expect(err).to.be.a('null');
                expect(result).to.be.a('object');
                done();
            });
        });
        it('should fail based on already existing content ID', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content"
            };
            digest.addContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Content id already exists");
                expect(result).to.be.a('null');
                done();
            });
        });
    });
    /**
     *
     * add to batch function here
     *
     *
     *
     */
    describe('Testing to add content to already existing batch', function () {
       it('Should create content in already existing batch', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid2,
                content: "this is the content"
            };
            digest.addContent(opts, function (err, result) {
                batchid = result.batch_id;
                expect(err).to.be.a('null');
                expect(result).to.be.a('object');
                done();
            });
        });
    });
    /**
     *
     * has function here
     *
     *
     *
     */
    describe('Check the has_content function', function () {
        it('Should return false for params', function (done) {
            var opts = {
                email_id: "thisiswrong",
                group_id: clientid,
                account_id: "uni",
                application_id: "thisiswrong",
                content_id: uuid1,
                content: "this is the content"
            };
            digest.hasContent(opts, function (err, result) {
                done();
            });
        });
        it('Should return true for params', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content"
            };
            digest.hasContent(opts, function (err, result) {
                done();
            });
        });
    });
    /**
     *
     * get function here
     *
     *
     *
     */
    describe('test the listbatchcontent function', function () {
        it('Should fail based on account_id', function (done) {
            var opts = {
                account_id: "thisiswrong",
                group_id: clientid,
                email_id: "1234",
                batch_id: batchid
            };
            group.listBatchContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "No curating content found");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('Should list content', function (done) {
            var opts = {
                account_id: "uni",
                group_id: clientid,
                email_id: "1234",
                batch_id: batchid

            };
            group.listBatchContent(opts, function (err, result) {
                console.log(err);
                expect(err).to.be.a('undefined');
                done();
            });
        });
    });
    describe('Check the get_content function', function () {
        it('fail auth based on email id', function (done) {
            var opts = {
                email_id: "thisiswrong",
                batch_id: batchid,
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content"
            };
            digest.getContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "No subscription by that name");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('fail based on content id', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                batch_id: batchid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: "thisiswrong",
                content: "this is the content"
            };
            digest.getContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "No content was found for that batch based on content id");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('fail based off the batch id', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content",
                batch_id: "thisiswrong"
            };
            digest.getContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "No content was found for that batch based on content id");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('Should get the batch', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content",
                batch_id: batchid
            };
            digest.getContent(opts, function (err, result) {
                expect(result).to.be.a('object');
                expect(err).to.be.a('undefined');
                done();
            });
        });
    });
    /**
     *
     * send function here
     *
     *
     *
     */
    describe('test the send function', function () {
        it('should fail based off the batch id', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content",
                batch_id: "thisiswrong"
            };
            digest.send(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "No batch with that ID found/curating.");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('send', function (done) {
            var opts = {
                email_id: "123411",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content",
                batch_id: batchid
            };
            digest.send(opts, function (err, result) {
                expect(err).to.be.a('null');
                expect(result).to.be.a('object');
                done();
            });

        });
    });
    /**
     *
     * send cont function here
     *
     *
     *
     */
    describe('testing send cont fuction', function () {
        it('should fail at batch id',function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content",
                batch_id: "thisiswrong"
            };
            digest.sendCont(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "No batch with that ID found/curating.");
                expect(result).to.be.a('undefined');
                done();
            });

        });
        it('send', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content",
                batch_id: batchid
            };
            digest.sendCont(opts, function (err, result) {
                expect(err).to.be.a('undefined');
                expect(result).to.be.a('object');
                done();
            });

        });
    });
    /**
     *
     * remove function here
     *
     *
     *
     */
    describe('remove content function', function () {
        it('should fail based off the batch id', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content",
                batch_id: "thisiswrong"
            };
            digest.removeContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "no content found with those params");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should fail based off the content id', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: "thisiswrong",
                content: "this is the content",
                batch_id: batchid
            };
            digest.removeContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "no content found with those params");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should remove content from batch', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: uuid1,
                content: "this is the content",
                batch_id: batchid
            };
            digest.removeContent(opts, function (err, result) {
                expect(result).to.be.a('object');
                expect(err).to.be.a('null');
                done();
            });
        });
    });
    describe('test the listgroup function', function () {
       it('should fail based on application_id', function (done) {
            var opts = {
                application_id: "thisiswrong",
                group_id: clientid,
                email_id: "1234",
                account_id: "uni"
            };
            group.listGroups(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Nothing returned");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should fail based on account id', function (done) {
            var opts = {
                application_id: "the-application-id",
                account_id: "thisiswrong",
                group_id: clientid,
                email_id: "1234"
            };
            group.listGroups(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Nothing returned");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should return list of groups', function (done) {
            var opts = {
                application_id: "the-application-id",
                group_id: clientid,
                email_id: "1234",
                account_id: "uni"
            };
            group.listGroups(opts, function (err, result) {
                expect(err).to.be.a('undefined');
                done();
            });
        });
    });
    describe('test the getgroupemail function', function () {
        it('should fail based on application_id', function (done) {
            var opts = {
                application_id: "thisiswrong",
                account_id: "uni",
                group_id: clientid,
                email_id: "1234"
            };
            group.listGroups(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Nothing returned");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should fail based on email_id', function (done) {
            var opts = {
                application_id: "the-application-id",
                account_id: "uni",
                group_id: clientid,
                email_id: "thisiswrong"
            };
            group.getGroupEmail(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "nothing for three part key");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should return email details', function (done) {
            var opts = {
                group_id: clientid,
                account_id: "uni",
                email_id: "1234"
            };
            group.getGroupEmail(opts, function (err, result) {
                console.log(err);
                expect(err).to.be.a('undefined');
                done();
            });
        });
    });
    describe('test the listgroupbatches function', function () {
       it('fail based on application id', function (done) {
           var opts = {
               application_id: "thisiswrong",
               account_id: "uni",
               group_id: clientid
           };
           group.listGroupBatches(opts, function (err, result) {
               expect(err).to.be.a('object');
               expect(err.errors[0]).to.have.property("property", "batch_id");
               expect(err.errors[0]).to.have.property("msg", "Error with batch ID.");
               expect(result).to.be.a('undefined');
               done();
           });
       });
        it('fail based on group id', function (done) {
            var opts = {
                application_id: "the-application-id",
                account_id: "uni",
                group_id: "thisiswrong"
            };
            group.listGroupBatches(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Error with batch ID.");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should list group batchs', function (done) {
            var opts = {
                application_id: "the-application-id",
                account_id: "uni",
                group_id: clientid
            };
            group.listGroupBatches(opts, function (err, result) {
                expect(err).to.be.a('undefined');
                done();
            });

        });
    });
    describe('test the listgroupemails function', function () {
        it('should fail based on account id', function (done) {
            var opts = {
                application_id: "the-application-id",
                account_id: "thisiswrong",
                group_id: clientid
            };
            group.listGroupEmails(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Error");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should fail based on groupid', function (done) {
            var opts = {
                application_id: "the-application-id",
                account_id: "uni",
                group_id: "thisiswrong12"
            };
            group.listGroupEmails(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Error");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should return a groups email subs', function (done) {
            var opts = {
                application_id: "the-application-id",
                account_id: "uni",
                group_id: clientid
            };
            group.listGroupEmails(opts, function (err, result) {
                expect(err).to.be.a('undefined');
                done();
            });
        });
    });
    describe('test the update function', function () {
        it('update successful', function (done) {
            var opts = {
                email_id: "1234",
                account_id: "uni",
                group_id: clientid,
                application_id: "the-application-id",
                content_id: uuid2,
                content: "this is the new content",
                batch_id: batchid
            };
            digest.updateContent(opts, function (err, result) {
                expect(result).to.be.a('object');
                expect(err).to.be.a('undefined');
                done();
            });
        });
        it('should fail based off the email id', function (done) {
            var opts = {
                email_id: "thisiswrong",
                account_id: "uni",
                group_id: clientid,
                application_id: "the-application-id",
                content_id: uuid2,
                content: "this is the content"
            };
            digest.updateContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid email sub");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should fail based off the application id', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "thisiswrong",
                content_id: uuid2,
                content: "this is the content"
            };
            digest.updateContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid email sub");
                expect(result).to.be.a('undefined');
                done();
            });
        });

        it('should fail based off the content id', function (done) {
            var opts = {
                email_id: "1234",
                group_id: clientid,
                account_id: "uni",
                application_id: "the-application-id",
                content_id: "thisiswrong",
                content: "this is the content"
            };
            digest.updateContent(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "could not update content");
                expect(result).to.be.a('undefined');
                done();
            });
        });
    });
    describe('Testing to update group', function () {
        it('Should fail based off email_id', function (done) {
            var opts = {
                doc_type: 'client-config',
                client_config_id: "",
                application_id: "the-application-id",
                email_id: "thisiswrong",
                "group_id": clientid,
                "account_id": "uni",
                subject: "custom-subject1",
                template: "custom-template1",
                frequency: "0 9 1-7 * 1 *",
                to: "jpiepkow@gmail.com",
                bcc: "jpiepkow@gmail.com"
            };
            group.updateGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid keys");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('Should fail based off application_id', function (done) {
            var opts = {
                doc_type: 'client-config',
                client_config_id: "",
                application_id: "thisiswrong",
                email_id: "1234",
                "group_id": clientid,
                "account_id": "uni",
                subject: "custom-subject1",
                template: "custom-template1",
                frequency: "0 9 1-7 * 1 *",
                to: "jpiepkow@gmail.com",
                bcc: "jpiepkow@gmail.com"
            };
            group.updateGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid keys");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('Should fail based off group_id', function (done) {
            var opts = {
                doc_type: 'client-config',
                client_config_id: "",
                application_id: "the-application-id",
                email_id: "1234",
                "group_id": "this is wrong",
                "account_id": "uni",
                subject: "custom-subject1",
                template: "custom-template1",
                frequency: "0 9 1-7 * 1 *",
                to: "jpiepkow@gmail.com",
                bcc: "jpiepkow@gmail.com"
            };
            group.updateGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid keys");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('Should update group', function (done) {
            var opts = {
                doc_type: 'client-config',
                client_config_id: "",
                application_id: "the-application-id",
                email_id: "1234",
                "group_id": clientid,
                "account_id": "uni",
                subject: "custom-subject1",
                template: "custom-template1",
                frequency: "0 9 1-7 * 1 *",
                to: "jpiepkow@gmail.com",
                bcc: "jpiepkow@gmail.com"
            };
            group.updateGroup(opts, function (err, result) {
                expect(err).to.be.a('undefined');
                expect(result).to.be.a('object');
                done();
            });
        });
    });
    describe('Testing to remove group', function () {
        it('Should fail based off email_id', function (done) {
            var opts = {
                email_id: "thisiswrong",
                account_id: "uni",
                application_id: "the-application-id",
                group_id: clientid
            };
            group.removeGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid keys");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('Should fail based off application_id', function (done) {
            var opts = {
                email_id: "1234",
                account_id: "uni",
                application_id: "thisiswrong",
                group_id: clientid
            };
            group.removeGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Not valid keys");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('Should fail based off group_id', function (done) {
            var opts = {
                email_id: "1234",
                account_id: "uni",
                application_id: "the-application-id",
                group_id: "thisiswrong"
            };
            group.removeGroup(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "failed to delete group");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('Should remove group', function (done) {
            var opts = {
                email_id: "1234",
                application_id: "the-application-id",
                account_id:"uni",
                group_id: clientid
            };
            group.removeGroup(opts, function (err, result) {
                expect(err).to.be.a('undefined');
                expect(result).to.be.a('object');
                done();
            });
        });
    });
    describe('test remove batch function',function(){
        it('should fail based off the batch_id',function(done){
            var opts = {
                application_id: "the-application-id",
                batch_id: "thisiswrong",
                account_id: "uni",
                group_id: clientid
            };
            digest.removeBatch(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Error with params passed in");
                expect(result).to.be.a('undefined');
                done();
            });
        });
        it('should fail based off the groupid',function(done){
            var opts = {
                application_id: "the-application-id",
                account_id: "uni",
                batch_id: batchid,
                group_id: "thisiswrong"
            };
            digest.removeBatch(opts, function (err, result) {
                expect(err).to.be.a('object');
                expect(err.errors[0]).to.have.property("property", "batch_id");
                expect(err.errors[0]).to.have.property("msg", "Error with params passed in");
                expect(result).to.be.a('undefined');
                done();
            });
        });
    });
});