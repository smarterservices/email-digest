'use strict';

/**
 * Handles all authentication related functions
 * @class authentication
 * @required async, AppUtils, ApiToken, Account, utils, fs, underscore, RedisClient
 */
var async = require('async');
var utils = require('./utils');
var fs = require('fs');
var _ = require('lodash');
var config = require('../config/');

module.exports = {

    /**
     * Authenticates an incoming API request.
     *
     * @async
     * @method authenticateRequest
     * @param {String} token The token that was provided in the request.
     * @param {String} account_id The account_id that was provided in the request. This typically will come from the URI.
     * @param {Object} [opts] Object that holds all the api token data.
     * @param {Function} callback A function for the callback accepting the following argument 'err, ApiToken'.
     * @example
     *    function(err, ApiToken){}
     */

    authenticateRequest: function (token, account_id, opts, callback) {

        // if opts is not provided.
        if (_.isFunction(opts)) {
            callback = opts;
            opts = {};
        }
        return callback(null, {});
    }

};