'use strict';

var moment = require('moment');
var utils = require('../utils');

module.exports = {

    ifCondition : function (condition, ifTrue, ifFalse) {
        if (condition) {
            return ifTrue;
        }
        return ifFalse;
    },
    formatTimeStamp : function (unix_ts, is_milliseconds) {
        return moment(unix_ts).toISOString(); // 2013-02-04T22:44:30.652Z
    },
    formatDeleteResponse : function (id) {
        return {
            deleted: true,
            id: id
        };
    },
    formatCollection: function (element, responseObj, outputCallback, callback) {
        var collection = responseObj.getData(),
            Paging = '',
            r = [],
            i = 0,
            response = {},
            data = '';
        if (responseObj.Paging) {
            Paging = responseObj.Paging;
        } else {
            Paging = utils.Paging();
        }

        if (Object.prototype.toString.call(collection) === '[object Array]') {
            for (i = 0; i < collection.length; i++) {
                data = collection[i];
                // the data will be contained in the value key when coming from couchbase...
                if (data.value) {
                    data = data.value;
                }
                r.push(outputCallback(data));
            }
        } else {
            r = collection;
        }

        response = {
            page: Paging.page,
            num_pages: Paging.num_pages,
            page_size: Paging.page_size,
            total: Paging.total,
            start: Paging.start,
            end: Paging.end,
            uri: Paging.uri,
            first_page_uri: Paging.first_page_uri,
            previous_page_uri: Paging.previous_page_uri,
            next_page_uri: Paging.next_page_uri,
            last_page_uri: Paging.last_page_uri
        };
        response[element] = r;

        responseObj.setData(response);

        if (callback) {
            return callback(responseObj);
        }
        return responseObj.getData();
    }

};