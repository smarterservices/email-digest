'use strict';

var OutputUtils = require('./output_utils');

module.exports = {

    send : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    add : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    deletecontent : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    getcontent : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    hascontent : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    updatecontent : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    addgroup : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    removeGroup : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    removeBatch : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    }

};

