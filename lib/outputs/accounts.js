'use strict';

module.exports = {
    addGroup : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    updateGroup : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    getGroup : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    removeGroup : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    listGroup : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    getGroupEmail : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    listContent : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    listGroupEmail : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    listGroupBatches : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    listEmailContent : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    }
};


