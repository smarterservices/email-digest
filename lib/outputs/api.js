'use strict';

var OutputUtils = require('./output_utils');

module.exports = {

    health : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);
        return callback(null, responseObj);

    },
    listaccount : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    },
    listemail : function (responseObj, callback) {
        var data = responseObj.getData();
        // set the data back in the object...
        responseObj.setData(data);

        return callback(null, responseObj);

    }

};
