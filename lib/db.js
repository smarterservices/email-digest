'use strict';

/**
 * Handles the connection to the MongoDB
 * @class db
 * @required config, mongodb
 */

var config = require('../config/'),
    MongoClient = require('mongodb').MongoClient;

var db_conn = null;
var mongo_conn = null;

module.exports = {

    mongo: function (callback) {
        var url;
        if (mongo_conn) { return callback(null, mongo_conn); }

        //placeholder: modify this-should come from a configuration source
        if (config.mongodb.username && config.mongodb.password) {
            url = 'mongodb://' + config.mongodb.username + ':' + config.mongodb.password + '@';
            url = url + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.database;
        } else {
            url = 'mongodb://' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.database;
        }

        MongoClient.connect(url, function (err, db) {

            if (err) { return callback('Error creating new connection ' + err, null); }
            console.log('connected to mongo!');
            mongo_conn = db;
            return callback(err, mongo_conn);
        });
    }
};