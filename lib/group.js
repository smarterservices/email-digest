'use strict';
/**
 * Handles all digest related functions
 * @class digest
 * @required events, async, util, db, config, message, moment, node-uuid, mandrill-api, cron-parser,iron-mq, messages
 */

var events = require('events'),
    async = require('async'),
    util = require('util'),
    db = require('./db.js'),
    utils = require('./utils.js'),
    Validator = require('validator.js'),
    Assert = Validator.Assert,
    Constraint = Validator.Constraint;
/**
 * Constructor for the Email digest class
 * @method EmailDigest
 * @param {Object} config The configuration information for the database.
 */

var EmailDigest = function (config) {
    if (!(this instanceof EmailDigest)) {
        return new EmailDigest(config);
    }
    this.config = config;
};
util.inherits(EmailDigest, events.EventEmitter);
/*jshint unused: true, node: true */
/*jslint unparam: true, node: true */


function valid(object, mode) {
    var ErrorObj = utils.Error(),
        constraint = {
            application_id: [new Assert([ 'listGroups', 'listGroupBatches', 'addGroup', 'removeGroup', 'updateGroup'],
                'applicaton_id must be required').Required(), new Assert().NotBlank()],
            account_id: [new Assert([ 'listGroups', 'getGroupEmail', 'getAccountGroup', 'listGroupEmails', 'listBatchContent', 'addGroup', 'removeGroup', 'updateGroup'],
                'account_id must be required').Required(), new Assert().NotBlank()],
            group_id: [new Assert([ 'getGroupEmail', 'getAccountGroup', 'listGroupEmails', 'listBatchContent', 'addGroup', 'removeGroup', 'updateGroup', 'listGroupBatches'],
                'group_id must be required').Required(), new Assert().NotBlank()],
            email_id: [new Assert([ 'getGroupEmail', 'getAccountGroup', 'listBatchContent', 'addGroup', 'removeGroup', 'updateGroup'],
                'email_id must be required').Required(), new Assert().NotBlank()],
            frequency: [new Assert([ 'addGroup', 'updateGroup']).Required(), new Assert().NotBlank()],
            to: [new Assert([ 'addGroup', 'updateGroup'], 'to must be required').Required(), new Assert().NotBlank()]
        };
    if (ErrorObj.addFromValidator(new Constraint(constraint).check(object, mode))) {
        return ErrorObj;
    }
    return null;
}
/**
 * Lists all of the group-subscription-config in array format that a account_
 * id is linked to
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.application_id -the application that is sending the email(ie. smarterproc)
 * @param {String} opts.account_id-The id of the university or client using the softwear(the highest level access)
 * @param {function} callback The function to call when complete.
 *
 * returned:
 --------------------------
 doc_type: 'group-subscription-config',
 group_config_id: '',
 application_id: opts.application_id,
 email_id: opts.email_id,
 group_id: opts.group_id,
 account_id: opts.account_id,
 subject: opts.subject,
 template: opts.template,
 frequency: opts.frequency,
 to: [to_values[0]],
 bcc: [bcc_values[0]]
 ---------------------------
 *
 */
EmailDigest.prototype.listGroups = function (opts, callback) {
    var ErrorObj = utils.Error(),
        returnthis = {};
    async.series([
        function (callback) {
            return callback(valid(opts, 'listGroups'));
        },
        function (callback) {
            //console.log('made it here');
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.find({
                    application_id: opts.application_id,
                    account_id: opts.account_id,
                    doc_type: 'group-subscription-config'
                }).toArray(function (err, res) {
                    if (res.length === 0) {
                        returnthis = res;
                        ErrorObj.addError('batch_id', 'Nothing returned');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    returnthis = res;
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, returnthis);
        });
};
EmailDigest.prototype.listAccounts = function (opts, callback) {
    var ErrorObj = utils.Error(),
        returnthis = {};
    async.series([
        function (callback) {
               //console.log('made it here');
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.find({
                    doc_type: 'account'
                }).toArray(function (err, res) {
                    if (res.length === 0) {
                        returnthis = res;
                        ErrorObj.addError('batch_id', 'Nothing returned');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    returnthis = res;
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, returnthis);
        });
};
EmailDigest.prototype.listEmail = function (opts, callback) {
    var ErrorObj = utils.Error(),
        returnthis = {};
    async.series([
        function (callback) {
                //console.log('made it here');
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.find({
                    doc_type: 'default-subscription-config'
                }).toArray(function (err, res) {
                    if (res.length === 0) {
                        returnthis = res;
                        ErrorObj.addError('batch_id', 'Nothing returned');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    returnthis = res;
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, returnthis);
        });
};
EmailDigest.prototype.listContent = function (opts, callback) {
    var ErrorObj = utils.Error(),
        returnthis = {};
    async.series([
        function (callback) {
                //console.log('made it here');
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.find({
                    doc_type: 'batch-content',
                    batch_id: opts.batch_id
                }).toArray(function (err, res) {
                    if (res.length === 0) {
                        console.log('nothing returned');
                        ErrorObj.addError('batch_id', 'Nothing returned');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    returnthis = res;
                    console.log(returnthis);
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, returnthis);
        });
};
/**
 * Returns 1 object of the default-subscription-config based on the email_id
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.account_id- The account the default subscription config belongs to. `
 * @param {String} opts.email_id-The unique id of the email batch config.
 * @param {String} opts.group_id-The unique id of the group
 * @param {function} callback The function to call when complete.
 * returned:
  --------------------------
 'doc_type': 'default-subscription-config',
 'application_id': 'the-application-id',
 'email_id': '1234',
 'name': 'Name of email',
 'template': 'mandrill_template_id',
 'subject': 'default-subject',
 'frequency': '0 9 1-7 * 1 *'
 ----------------------------
 *
 */
EmailDigest.prototype.getGroupEmail = function (opts, callback) {
    var ErrorObj = utils.Error(),
        returnthis = {};
    async.series([
        function (callback) {
            return callback(valid(opts, 'getGroupEmail'));
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    account_id: opts.account_id,
                    group_id: opts.group_id,
                    email_id: opts.email_id,
                    doc_type: 'group-subscription-config'
                }, function (err, res) {
                    if (!res) {
                        ErrorObj.addError('batch_id', 'nothing for three part key');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    collection.findOne({
                        email_id: opts.email_id,
                        doc_type: 'default-subscription-config'
                    }, function (err, r) {
                        if (!r) {
                            ErrorObj.addError('batch_id', 'internal error');
                            ErrorObj.errorCode = 'InvalidInput';
                        }
                        returnthis = r;
                        callback();
                    });
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, returnthis);
        });
};
/**
 * Returns 1 object of the group-subscription-config based on the account_id
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.account_id- The account the default subscription config belongs to. `
 * @param {String} opts.email_id-The unique id of the email batch config.
 * @param {String} opts.group_id-The unique id of the group
 * @param {function} callback The function to call when complete.
 *
 *
 */
EmailDigest.prototype.getAccountGroup = function (opts, callback) {
    var ErrorObj = utils.Error(),
        returnthis = {};
    async.series([
        function (callback) {
            return callback(valid(opts, 'getAccountGroup'));
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    account_id: opts.account_id,
                    group_id: opts.group_id,
                    email_id: opts.email_id,
                    doc_type: 'group-subscription-config'
                }, function (err, res) {
                    if (!res) {
                        ErrorObj.addError('batch_id', 'nothing returned');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    returnthis = res;
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, returnthis);
        });
};

/**
 * returns all email subs a group belongs to
 *
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.application_id -the application that is sending the email(ie. smarterproc)
 * @param {String} opts.account_id-The id of the university or client using the softwear(the highest level access)
 * @param {String} opts.group_id-The unique id of a group config file.
 * @param {function} callback The function to call when complete.
 * returned:
 --------------------------
emailid1, emailid2 emailid3
 ----------------------------
 *
 */
EmailDigest.prototype.listGroupEmails = function (opts, callback) {
    var ErrorObj = utils.Error(),
        i = 0,
        returnthis = [];
    async.series([
        function (callback) {
            return callback(valid(opts, 'listGroupEmails'));
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.find({
                    doc_type: 'group-subscription-config',
                    account_id: opts.account_id,
                    group_id: opts.group_id
                }).toArray(function (err, res) {
                    if (res.length === 0) {
                        ErrorObj.addError('batch_id', 'Error');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    for (i = 0; i <= (res.length - 1); i++) {
                        returnthis[i] = res[i].email_id;
                    }
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, returnthis);
        });
};
/**
 * lists all of the email-batch documents that belong to a group(sent or still curating)
 *
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.group_id-The unique id of a group config file.
 * @param {String} opts.application_id -the application that is sending the email(ie. smarterproc)
 * @param {function} callback The function to call when complete.
 *
 */
EmailDigest.prototype.listGroupBatches = function (opts, callback) {
    console.log('made it here');
    var ErrorObj = utils.Error(),
        returnthis = {};
    async.series([
        function (callback) {
            return callback(valid(opts, 'listGroupBatches'));
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.find({
                    application_id: opts.application_id,
                    group_id: opts.group_id,
                    doc_type: 'email-batch'
                }).toArray(function (err, res) {
                    if (res.length === 0) {
                        returnthis = res;
                        ErrorObj.addError('batch_id', 'Error with batch ID.');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    returnthis = res;
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, returnthis);
        });
};
/**
 * list all of the batch content for a single batch id
 *
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id-The unique id of the email sub
 * @param {String} opts.account_id-The id of the university or client using the softwear(the highest level access)
 * @param {String} opts.group_id-The unique id of a group config file.
 * @param {function} callback The function to call when complete.
 * returned:
 --------------------------
 returns a array of the batch content
 ----------------------------
 *
 */
EmailDigest.prototype.listBatchContent = function (opts, callback) {
    var ErrorObj = utils.Error(),
        i = 0,
        count = '',
        local = {},
        itemsArray = [];
    async.series([
        function (callback) {
            return callback(valid(opts, 'listBatchContent'));
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    account_id: opts.account_id,
                    group_id: opts.group_id,
                    email_id: opts.email_id,
                    status: 'curating',
                    doc_type: 'email-batch'
                }, function (err, r) {
                    if (!r) {
                        ErrorObj.addError('batch_id', 'No curating content found');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    local.batch = r.batch_id;
                    //console.log(local.batch);
                    callback();
                });
            });
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.find({
                    batch_id: local.batch,
                    doc_type: 'batch-content'
                }).toArray(function (err, res) {if (res.length === 0) {
                    ErrorObj.addError('batch_id', 'Error with batch ID.');
                    ErrorObj.errorCode = 'InvalidInput';
                    return callback(ErrorObj);
                }
                    for (i = 0, count = res.length; i < count; i++) {
                        itemsArray.push(res[i]);
                    }
                    callback();
                    });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, itemsArray);
        });
};
/**
 * adds a new group - config
 *
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id-The unique id of the email batch config.
 * @param {String} opts.application_id -the application that is sending the email(ie. smarterproc)
 * @param {String} opts.account_id -
 * @param {String} opts.to - a list of email addresses(to) seperated by ,
 * @param {String} opts.bcc - a list of email addresses(bcc) seperated by ,
 * @param {String} opts.frequency - the send frequency
 * @param {String} opts.template - the template name
 * @param {String} opts.subject - the cutome subject name
 * @param {String} opts.group_id - the unique group id
 * @param {function} callback The function to call when complete.
 * returned:
 --------------------------
 add new group-subscription-config
 ----------------------------
 *
 */
EmailDigest.prototype.addGroup = function (opts, callback) {
    var ErrorObj = utils.Error(),
        toarray = [],
        bccarray = [];
    async.series([
        function (callback) {
            return callback(valid(opts, 'addGroup'));
        },
            //check the AppId against ClientId to see if exists
        function (callback) {
            if (opts.bcc === '') {
                opts.bcc = null;
            }
            if (opts.bcc) {
                bccarray = opts.bcc.split(',');
            }
            toarray = opts.to.split(',');
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    email_id: opts.email_id,
                    application_id: opts.application_id,
                    doc_type: 'default-subscription-config'
                }, function (err, item) {
                    if (!item) {
                        ErrorObj.addError('batch_id', 'Not valid keys');
                        ErrorObj.errorCode = 'InternalError';
                        return callback(ErrorObj);
                    }
                    callback();

                });
            });
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    group_id: opts.group_id,
                    doc_type: 'group-subscription-config'
                }, function (err, item) {
                    if (!item) {
                        callback();
                    } else {
                        ErrorObj.addError('batch_id', 'Client ID already exists');
                        ErrorObj.errorCode = 'InternalError';
                        return callback(ErrorObj);
                    }
                });
            });
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.insert(
                    {
                        doc_type: 'group-subscription-config',
                        group_config_id: '',
                        application_id: opts.application_id,
                        email_id: opts.email_id,
                        group_id: opts.group_id,
                        account_id: opts.account_id,
                        subject: opts.subject,
                        template: opts.template,
                        frequency: opts.frequency,
                        to: toarray,
                        bcc: bccarray
                    },
                    function (err, result) {
                        if (!err) {
                            callback();
                            db.mongo(function (err, conn) {
                                collection.findOne({
                                    name: opts.account_id,
                                    doc_type: 'account'
                                }, function (err, item) {
                                    if (!item) {
                                        collection.insert(
                                            {
                                                doc_type: 'account',
                                                name: opts.account_id
                                            },
                                            function (err, result) {
                                                if (!err) {
                                                    console.log('account created');
                                                } else {
                                                    ErrorObj.addError('batch_id', 'Failed to create account');
                                                    ErrorObj.errorCode = 'InternalError';
                                                    return callback(ErrorObj);
                                                }
                                            }
                                        );
                                    }
                                });
                            });
                        } else {
                            ErrorObj.addError('batch_id', 'Failed to push array of values');
                            ErrorObj.errorCode = 'InternalError';
                            return callback(ErrorObj);
                        }
                    }
                );
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, {'created_for' : opts.group_id});
        });
};
/**
 * remove a group - config
 *
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id-The unique id of the email batch config.
 * @param {String} opts.application_id -the application that is sending the email(ie. smarterproc)
 * @param {function} callback The function to call when complete.
 * returned:
 ---------------------------
 remove a group-subscription-config
 ----------------------------
 *
 */
EmailDigest.prototype.removeGroup = function (opts, callback) {
    var ErrorObj = utils.Error();
    async.series([
        function (callback) {
            return callback(valid(opts, 'removeGroup'));
        },
            //check the AppId against ClientId to see if exists
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    email_id: opts.email_id,
                    application_id: opts.application_id,
                    doc_type: 'default-subscription-config'
                }, function (err, item) {
                    if (!item) {
                        ErrorObj.addError('batch_id', 'Not valid keys');
                        ErrorObj.errorCode = 'InternalError';
                        return callback(ErrorObj);
                    }
                    callback();
                });
            });
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.remove(
                    {
                        doc_type: 'group-subscription-config',
                        email_id: opts.email_id,
                        application_id: opts.application_id,
                        account_id: opts.account_id,
                        group_id: opts.group_id
                    },
                    function (err, r) {
                        if (r.result.n !== 0) {
                            callback();
                        } else {
                            //console.log(r.result.n);
                            ErrorObj.addError('batch_id', 'failed to delete group');
                            ErrorObj.errorCode = 'InternalError';
                            return callback(ErrorObj);
                        }
                    }
                );
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, {'deleted_for' : opts.group_id});
        });
};
/**
 * make changes to a group-subscription-config
 *
 * @method add_content
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id-The unique id of the email batch config.
 * @param {String} opts.application_id -the application that is sending the email(ie. smarterproc)
 * @param {String} opts.to - a list of email addresses(to) seperated by ,
 * @param {String} opts.bcc - a list of email addresses(bcc) seperated by ,
 * @param {String} opts.frequency - the send frequency
 * @param {String} opts.template - the template name
 * @param {String} opts.subject - the cutome subject name
 * @param {String} opts.group_id - the unique group id
 * @param {function} callback The function to call when complete.
 * returned:
 ---------------------------
 make changes to a group config
 ----------------------------
 *
 */
EmailDigest.prototype.updateGroup = function (opts, callback) {
    var ErrorObj = utils.Error(),
        toarray = [],
        bccarray = [];
    async.series([
        function (callback) {
            return callback(valid(opts, 'updateGroup'));
        },
            //check the AppId against ClientId to see if exists
        function (callback) {
            if (opts.bcc === '') {
                opts.bcc = null;
            }
            if (opts.bcc) {
                bccarray = opts.bcc.split(',');
            }
            toarray = opts.to.split(',');
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    email_id: opts.email_id,
                    application_id: opts.application_id,
                    doc_type: 'default-subscription-config'
                }, function (err, item) {
                    if (!item) {
                        ErrorObj.addError('batch_id', 'Not valid keys');
                        ErrorObj.errorCode = 'InternalError';
                        return callback(ErrorObj);
                    }
                    callback();
                });
            });
        },
        function (callback) {
            console.log(opts.application_id + opts.group_id + opts.email_id);
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.update({
                    doc_type: 'group-subscription-config',
                    email_id: opts.email_id,
                    application_id: opts.application_id,
                    group_id: opts.group_id
                }, {$set: {
                    doc_type: 'group-subscription-config',
                    group_config_id: '',
                    application_id: opts.application_id,
                    email_id: opts.email_id,
                    group_id: opts.group_id,
                    account_id: opts.account_id,
                    subject: opts.subject,
                    template: opts.template,
                    frequency: opts.frequency,
                    to: toarray,
                    bcc: bccarray
                }}, function (err, done) {
                    if (done.result.n === 1) {
                        callback();
                    } else {
                        ErrorObj.addError('batch_id', 'Not valid keys');
                        ErrorObj.errorCode = 'InternalError';
                        return callback(ErrorObj);
                    }
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, {'updated_for' : opts.group_id});
        });
};
module.exports = EmailDigest;


