'use strict';
/**
 * Handles all digest related functions
 * @class digest
 * @required events, async, util, db, config, message, moment, node-uuid, mandrill-api, cron-parser,iron-mq, messages
 */

var events = require('events'),
    async = require('async'),
    util = require('util'),
    db = require('./db.js'),
    utils = require('./utils.js'),
    config = require('../config/'),
    Validator = require('validator.js'),
    Constraint = Validator.Constraint,
    Assert = Validator.Assert,
    moment = require('moment'),
    uuid = require('node-uuid'),
    mandrill = require('mandrill-api/mandrill'),
    mandrill_client = new mandrill.Mandrill(config.mandrill.api_key),
    parser = require('cron-parser'),
    iron_mq = require('iron_mq'),
    imq = new iron_mq.Client({token: config.iron.mq.token, project_id: config.iron.mq.project_id}),
    queue = imq.queue('courier-' + process.env.NODE_ENV + '-messages');
/**
 * Constructor for the Email digest class
 * @method EmailDigest
 * @param {Object} config The configuration information for the database.
 */

var EmailDigest = function (config) {
    if (!(this instanceof EmailDigest)) {
        return new EmailDigest(config);
    }
    this.config = config;
};
util.inherits(EmailDigest, events.EventEmitter);

/*jshint unused: true, node: true */
/*jslint unparam: true, node: true */
function valid(object, mode) {
    var ErrorObj = utils.Error(),
        constraint = {
            application_id: [
                new Assert(['addContent'], 'Application_id must be required.').Required(),
                new Assert().NotBlank()
            ],
            account_id: [
                new Assert(['addContent', 'updateContent', 'removeBatch', 'getContent'], 'Account Id is required').Required(),
                new Assert().NotBlank()
            ],
            group_id: [
                new Assert(['addContent', 'removeBatch'], 'group_id must be required').Required(),
                new Assert().NotBlank()
            ],
            email_id: [
                new Assert(['addContent', 'updateContent', 'getContent'], 'email_id must be required').Required(),
                new Assert().NotBlank()
            ],
            batch_id: [
                new Assert(['removeBatch', 'getContent', 'send', 'sendCont'], 'batch_id must be required').Required(),
                new Assert().NotBlank()
            ]
        };

    // now run all the checks...
    if (ErrorObj.addFromValidator(new Constraint(constraint).check(object, mode))) {
        return ErrorObj;
    }
    return null;
}
/**
 * adds content to a batch
 * @method addContent
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id -The 'type' of email that is going to be sent. ex uppcomming-sessions
 * @param {String} opts.group_id -The id of the client account. ex jordanpiepkow
 * @param {String} opts.account_id -The id of the account type. ex aubern-univ
 * @param {String} opts.application_id -The id of the actual application. smarter-service
 * @param {String} opts.content_id -The unique id that is associated with this content.
 * @param {String} opts.content -The content to be added to batch.
 * @param {function} callback The function to call when complete.
 *
 */
EmailDigest.prototype.addContent = function (opts, callback) {
    var ErrorObj = utils.Error(),
        local = {},
        time = {};


    async.series([
        function (callback) {
            return callback(valid(opts, 'addContent'));
        },
        function (callback) {
            //console.log('Made it here.');
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    email_id: opts.email_id,
                    doc_type: 'group-subscription-config'
                }, function (err, item) {
                    if (!item) {
                        ErrorObj.addError('batch_id', 'No match on email type found');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    collection.findOne({
                        application_id: opts.application_id,
                        account_id: opts.account_id,
                        group_id: opts.group_id,
                        email_id: opts.email_id,
                        doc_type: 'group-subscription-config'
                    }, function (err, client) {
                        if (!client) {
                            ErrorObj.addError('batch_id', 'Client does not exist for this Application/that email type');
                            ErrorObj.errorCode = 'InvalidInput';
                            return callback(ErrorObj);
                        }
                                    //console.log('EmailID and ClientID are right');
                        callback();
                    });
                });
            });
        },
            //set frequency override if client included frequency
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    group_id: opts.group_id,
                    doc_type: 'group-subscription-config',
                    frequency: {$exists: true}
                }, function (err, item) {
                    if (!item) {
                            //callback('No Frequency');
                        collection.findOne({
                            email_id: opts.email_id,
                            doc_type: 'group-subscription-config'
                        }, function (err, freq) {
                            local.frequency = freq.frequency;
                                //console.log(local.frequency);
                            callback();
                        });
                    } else {
                            //callback('Frequency found')
                        local.frequency = item.frequency;
                            //console.log(local.frequency);
                        callback();
                    }
                });
            });
        },
        function (callback) {
            try {
                local.parsedtime = parser.parseExpression(local.frequency);
                local.nexttime = local.parsedtime.next();
                local.unixnexttime = moment(local.nexttime).unix();
                //console.log(local.unixnexttime);
                callback();
            } catch (err) {
                ErrorObj.addError('batch_id', 'Error!!!: ' + err.message);
                ErrorObj.errorCode = 'InternalError';
                return callback(ErrorObj);
            }
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    content_id: opts.content_id
                }, function (err, item) {
                    if (!item) {
                        callback();
                    } else {
                        ErrorObj.addError('batch_id', 'Content id already exists');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                });
            });
        },
        //look up message by next send date.
        function (callback) {
            local.content_id = opts.content_id;
            //console.log(local.contentid);
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    group_id: opts.group_id,
                    account_id: opts.account_id,
                    email_id: opts.email_id,
                    send_at: local.unixnexttime
                }, function (err, item) {
                    if (!item) {
                        local.newuuid = uuid.v1();
                        collection.insert(
                            {
                                doc_type: 'email-batch',
                                batch_id: local.newuuid,
                                send_at: local.unixnexttime,
                                application_id: opts.application_id,
                                email_id: opts.email_id,
                                group_id: opts.group_id,
                                account_id: opts.account_id,
                                status: 'curating', // defaults to curating, the other valid status is 'sent'.
                                content_ids: [local.content_id]
                            }
                        );
                        local.batch = {
                            doc_type: 'batch-content',
                            batch_id: local.newuuid,
                            content_id: local.content_id,
                            content: opts.content
                        };
                        collection.insert(local.batch);
                           //figure out if send time is more then a week out
                        time.nowm = moment();
                        time.now = (time.nowm / 1000);
                        time.week_second = 604800;
                        if (time.now - local.unixnexttime >= time.week_second) {
                            //console.log('Time now:' + time.now);
                            //console.log('Send time' + local.unixnexttime);
                            time.send_time = time.week_second;
                        } else {
                            time.send_time = (local.unixnexttime - time.now);
                        }
                        //console.log('Send Time' + time.send_time);
                        //------------------------------------------------
                        var message_payload = {
                                batch_id: local.newuuid,
                                application_id: opts.application_id
                            },
                            //console.log('message_payload' + message_payload);
                            // now create the props to send to the queue, we need to stringify the message payload b/c the body expects a string.
                            q_props = {
                                body: JSON.stringify(message_payload),
                                delay: parseInt(time.send_time, 10) // for testing we are delaying just 10 seconds...
                            };
                        //console.log('props' + q_props.body);
                        //console.log('Time' + q_props.delay);
                        queue.post(q_props, function (err, message_id) {
                            if (!err) {
                                //console.log(parseInt(time.send_time, 10));
                                // we will prob want to store this message_id with the batch so we have it...
                                //console.log('Message Id: ' + message_id);
                                collection.update({
                                    batch_id: local.newuuid,
                                    doc_type: 'email-batch',
                                    status: 'curating'
                                }, {$set: {ironid: message_id}}, {upsert: true}, function (err, done) {
                                    if (!done) {
                                        ErrorObj.addError('batch_id', 'Error updating batch with ironid');
                                        ErrorObj.errorCode = 'InternalError';
                                        return callback(ErrorObj);
                                    }
                                    callback();
                                });
                            } else {
                                ErrorObj.addError('batch_id', err);
                                ErrorObj.errorCode = 'InternalError';
                                return callback(ErrorObj);
                            }
                        });
                    } else {
                            //console.log('Join batch');
                        local.batch = {
                            doc_type: 'batch-content',
                            batch_id: item.batch_id,
                            content_id: local.content_id,
                            content: opts.content
                        };
                        collection.update({
                            batch_id: item.batch_id,
                            doc_type: 'email-batch'
                        }, {$push: {content_ids: local.content_id}});
                        collection.insert(local.batch);
                        callback();
                    }
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err, null);
            }
            return callback(null, local.batch);
        });
};
/**
 * removes content from batch
 * @method removeContent
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id -The 'type' of email that is going to be sent.
 * @param {String} opts.group_id -The id of the client account.
 * @param {String} opts.application_id -The id of the actual application.
 * @param {String} opts.content_id -The unique id that is associated with this content.
 */
EmailDigest.prototype.removeContent = function (opts, callback) {
    var ErrorObj = utils.Error(),
        local = {};
    this.getContent(opts, function (err, item) {
        if (!err) {
            local = {
                batch_id: item.batch_id,
                content_id: item.content_id,
                content: item.content
            };
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.remove({
                    batch_id: local.batch_id,
                    content_id: local.content_id,
                    doc_type: 'batch-content'
                }, function (err, remove) {
                    if (!err) {
                        collection.update({
                            batch_id: local.batch_id,
                            doc_type: 'email-batch'
                        }, {$pull: {content_ids: local.content_id}});
                    } else {
                        ErrorObj.addError('batch_id', 'Remove failed');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                });
            });
            return callback(null, {itworked: 'true'});
        }
        ErrorObj.addError('batch_id', 'no content found with those params');
        ErrorObj.errorCode = 'InvalidInput';
        return callback(ErrorObj);
    });
};

/**
 * Update the content of a batch
 * @method hasContent
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id -The 'type' of email that is going to be sent.
 * @param {String} opts.content -The content that will be used to update the batch
 * @param {String} opts.application_id -The id of the actual application.
 * @param {String} opts.content_id -The unique id that is associated with this content.
 */
EmailDigest.prototype.updateContent = function (opts, callback) {
    var ErrorObj = utils.Error();
    async.series([
        function (callback) {
            //console.log(valid(opts, 'updateContent'));
            return callback(valid(opts, 'updateContent'));
        },
        function (callback) {
            //console.log('---> Hi');
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    email_id: opts.email_id,
                    application_id: opts.application_id,
                    doc_type: 'group-subscription-config'
                },
                    function (err, item) {
                        if (!item) {
                            ErrorObj.addError('batch_id', 'Not valid email sub');
                            ErrorObj.errorCode = 'InvalidInput';
                            return callback(ErrorObj);
                        }
                        callback();
                    });
            });
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.update({
                    doc_type: 'batch-content',
                    content_id: opts.content_id
                }, {$set: {content: opts.content}}, function (err, done) {
                    if (done.result.n === 0) {
                        ErrorObj.addError('batch_id', 'could not update content');
                        ErrorObj.errorCode = 'InternalError';
                        return callback(ErrorObj);
                    }
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, {itworked: true});
        });
};
/**
 * Removes a batch and all its contents
 * @method removeBatch
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.account_id -The id of the account
 * @param {String} opts.group_id -The id of the group account.
 * @param {String} opts.application_id -The id of the actual application.
 * @param {String} opts.batch_id -The id of the batch
 */
EmailDigest.prototype.removeBatch = function (opts, callback) {
    var ErrorObj = utils.Error(),
        i = 0;
    async.series([
        function (callback) {
            return callback(valid(opts, 'removeBatch'));
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                console.log(opts.application_id + opts.account_id + opts.group_id + opts.batch_id);
                collection.findOne({
                    batch_id: opts.batch_id,
                    group_id: opts.group_id,
                    account_id: opts.account_id,
                    doc_type: 'email-batch',
                    status: 'curating'
                }, function (err, res) {
                    if (!res) {
                        ErrorObj.addError('batch_id', 'Error with params passed in');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    for (i =  0; i <= res.content_ids.length; i++) {
                        collection.remove(
                            {
                                doc_type: 'batch-content',
                                batch_id: opts.batch_id,
                                content_id: res.content_ids[i]
                            }
                        );
                    }
                    collection.remove(
                        {
                            doc_type: 'email-batch',
                            batch_id: opts.batch_id
                        },
                        function (err, r) {
                            if (err) {
                                ErrorObj.addError('batch_id', 'Error with params passed in');
                                ErrorObj.errorCode = 'InvalidInput';
                                return callback(ErrorObj);
                            }
                        }
                    );
                    callback();
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, {worked: 'Batch deleted'});
        });
};
/**
 * Check if a batch as content
 * @method hasContent
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id -The 'type' of email that is going to be sent.
 * @param {String} opts.group_id -The id of the client account.
 * @param {String} opts.application_id -The id of the actual application.
 * @param {String} opts.content_id -The unique id that is associated with this content.
 */
EmailDigest.prototype.hasContent = function (opts, callback) {
    var ErrorObj = utils.Error();
    this.getContent(opts, function (err, item) {
        if (!item) {
            ErrorObj.addError('batch_id', 'No content was found');
            ErrorObj.errorCode = 'ResourceNotFound';
            return callback(ErrorObj);
        }
            //console.log('false');
        callback(null, false);
    });
};
/**
 * Pulls and returns the content of a batch
 * @method getContent
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.email_id -The 'type' of email that is going to be sent.
 * @param {String} opts.group_id -The id of the client account.
 * @param {String} opts.application_id -The id of the actual application.
 * @param {String} opts.content_id -The unique id that is associated with this content.
 */
EmailDigest.prototype.getContent = function (opts, callback) {
    var ErrorObj = utils.Error(),
        result = {};
    async.series([
            //check the AppId against ClientId to see if exists
        function (callback) {
            return callback(valid(opts, 'getContent'));
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    email_id: opts.email_id,
                    doc_type: 'group-subscription-config'
                }, function (err, item) {
                    if (!item) {
                        //console.log(item);
                        ErrorObj.addError('batch_id', 'No subscription by that name');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                            //console.log('found a match');
                    collection.findOne({
                        batch_id: opts.batch_id,
                        content_id: opts.content_id,
                        doc_type: 'batch-content'
                    }, function (err, client) {
                        if (!client) {
                            ErrorObj.addError('batch_id', 'No content was found for that batch based on content id');
                            ErrorObj.errorCode = 'ResourceNotFound';
                            return callback(ErrorObj);
                        }
                        result.batch_id = client.batch_id;
                        result.content_id = client.content_id;
                        result.content = client.content;
                        callback();
                    });
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, result);
        });
};
/**
 * Sends the batch
 * @method send
 * @param {Object} opts Object that holds all the variables.
 * @param {String} opts.batch_id - The batch that should be sent.
 * @param {String} opts.application_id - The application_id that the batch belongs to.
 */
EmailDigest.prototype.send = function (opts, callback) {
    console.log('sent');
    //console.log(opts);
    var ErrorObj = utils.Error(),
        sendback = {},
        time = {},
        _this = this;
    this.opts = opts;
    async.series([
        function (callback) {
            return callback(valid(opts, 'send'));
        },
            //check if time is =<now or check if time remaining is greater then a week
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                collection.findOne({
                    batch_id: opts.batch_id,
                    doc_type: 'email-batch'
                }, function (err, timeid) {
                    if (!timeid) {
                        //console.log('there was an error' + err);
                        ErrorObj.addError('batch_id', 'No batch with that ID found/curating.');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    sendback.application_id = timeid.application_id;
                    time.send = timeid.send_at;
                    //console.log('the time is to send is' + time.send);
                    callback();
                });
            });
        },
        function (callback) {
            //console.log('makes it thais far!!!!!!');
            time.nowm = moment();
            time.now = (time.nowm / 1000);
            //console.log('the time now is' + time.now);
            time.week_second = 604800;
            //Send time more then 1 week out(set send time to exactly a week)
            if (time.now - time.send >= time.week_second) {
                time.send_time = time.week_second;
                //console.log('to send is' + time.send_time);
            } else {
                time.send_time = (time.send - time.now);
                    //console.log('to send is' + time.send_time);
            }
            //console.log('parsed time' + parseInt(time.send_time, 10));
            if (time.send <= time.now) {
                _this.sendCont(opts, function (err, item) {
                    if (!err) {
                        console.log('true');
                        callback(err, item);
                    } else {
                        console.log('false');
                        callback(err, item);
                    }
                });
                sendback.wassent = 'Yes';
            } else {
                sendback.wassent = 'No, sent back to iron';
                sendback.batch_id = opts.batch_id;
                var message_payload = {
                        batch_id: opts.batch_id,
                        application_id: opts.application_id
                    },
                    //console.log('message_payload' + message_payload);
                    // now create the props to send to the queue, we need to stringify the message payload b/c the body expects a string.
                    q_props = {
                        body: JSON.stringify(message_payload),
                        delay: parseInt(time.send_time, 10) // for testing we are delaying just 10 seconds...
                    };
                queue.post(q_props, function (err, message_id) {
                    if (!err) {
                        //console.log(parseInt(time.send_time, 10));
                        // we will prob want to store this message_id with the batch so we have it...
                        //console.log('Message Id: ' + message_id);
                        db.mongo(function (err, conn) {
                            var collection = conn.collection('EmailDigest');
                            collection.update({
                                batch_id: opts.batch_id,
                                doc_type: 'email-batch',
                                status: 'curating'
                            }, {$set: {ironid: message_id}}, {upsert: true}, function (err, done) {
                                if (!done) {
                                    ErrorObj.addError('batch_id', 'error updating batch with ironid');
                                    ErrorObj.errorCode = 'InternalError';
                                    return callback(ErrorObj);
                                }
                                        //console.log('message sent to iron');
                                callback();
                            });
                        });
                    } else {
                        ErrorObj.addError('batch_id', err);
                        ErrorObj.errorCode = 'InternalError';
                        return callback(ErrorObj);
                    }
                });
            }
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(null, sendback);
        });
};
EmailDigest.prototype.sendCont = function (opts, callback) {
    var ErrorObj = utils.Error(),
        template_name = 'courier-general-skin',
        template_content = [
            {'name': 'content', 'content': 'this is template content'}
        ],
        message = {
            'global_merge_vars': [],
            'subject': 'example subject',
            'from_email': 'fallback@example.com',
            'from_name': 'fallback',
            'to': [{
                'email': 'sample',
                'name': 'Recipient Name',
                'type': 'to'
            }]
        },
        i = 0,
        count = '',
        itemsArray = [],
        email = '';
    async.series([
        function (callback) {
            return callback(valid(opts, 'sendCont'));
        },
        function (callback) {
            db.mongo(function (err, conn) {
                var collection = conn.collection('EmailDigest');
                //find batch based on id that is status:curating
                collection.findOne({
                    batch_id: opts.batch_id,
                    application_id: opts.application_id,
                    doc_type: 'email-batch',
                    status: 'curating'
                }, function (err, item) {
                    if (!item) {
                        ErrorObj.addError('batch_id', 'No batch with that ID found/curating.');
                        ErrorObj.errorCode = 'InvalidInput';
                        return callback(ErrorObj);
                    }
                    //find client id in order to get send address's
                    collection.findOne({
                        doc_type: 'default-subscription-config',
                        email_id: item.email_id
                    }, function (err, email) {
                        if (!email) {
                            ErrorObj.addError('client ID', 'Error with client ID.');
                            ErrorObj.errorCode = 'InvalidInput';
                            return callback(ErrorObj);
                        }
                        if (email.from_email) {
                            //console.log('made it to email change' + email.from_email);
                            message.from_name = email.from_email;
                            message.from_email = email.from_email;
                        }
                    });
                    collection.findOne({
                        group_id: item.group_id,
                        doc_type: 'group-subscription-config'
                    }, function (err, id) {
                        if (!id) {
                            ErrorObj.addError('client ID', 'Error with client ID.');
                            ErrorObj.errorCode = 'InvalidInput';
                            return callback(ErrorObj);
                        }
                        //console.log(id.subject);
                        if (id.subject) {
                            message.subject = id.subject;
                            //console.log('Subject was changed to' + message.subject);
                        }
                        var to_count = 0,
                            to_done = 0,
                            bcc_count = 0,
                            bcc_done = 0;
                        while (to_done !== 1) {
                            if (id.to[to_count] !== undefined) {
                                console.log('makesit1' + (bcc_count + to_count));
                                            //console.log(id.to[to_count]);
                                message.to[to_count] = {
                                    email: id.to[to_count],
                                    name: id.to[to_count],
                                    type: 'to'
                                };
                                //console.log(message.to[to_count]);
                                to_count++;
                            } else {
                                to_done = 1;
                            }
                        }
                        while (bcc_done !== 1) {
                            if (id.bcc[bcc_count] !== undefined) {
                                console.log('makesit2' + (bcc_count + to_count));
                                message.to[bcc_count + to_count] = {
                                    email: id.bcc[bcc_count],
                                    name: id.bcc[bcc_count],
                                    type: 'bcc'
                                };
                                //console.log(message.to[bcc_count + to_count]);
                                bcc_count++;
                            } else {
                                bcc_done = 1;
                            }
                        }

                    //find all the batch content and store into a string with html tags
                        collection.find({
                            batch_id: opts.batch_id,
                            doc_type: 'batch-content'
                        }).toArray(function (err, res) {
                            if (!res) {
                                ErrorObj.addError('batch_id', 'Error with batch ID.');
                                ErrorObj.errorCode = 'InvalidInput';
                                return callback(ErrorObj);
                            }
                            for (i = 0, count = res.length; i < count; i++) {
                                itemsArray.push(res[i]);
                            }
                            for (i = 0, count = res.length; i < count; i++) {
                                email += itemsArray[i].content + '<br> <hr>';
                            }
                            message.global_merge_vars.push({
                                'name': 'content',
                                'content': email
                            });
                        //console.log(template_content);
                            console.log('--------------------------------');
                            console.log(message);
                            console.log('--------------------------------');
                                    //this is where the message is being sent out
                            mandrill_client.messages.sendTemplate({
                                'template_name': template_name,
                                'template_content': template_content,
                                'message': message
                            }, function (result) {
                                console.log(result);
                                collection.update({
                                    batch_id: opts.batch_id,
                                    doc_type: 'email-batch',
                                    status: 'curating'
                                }, {$set: {status: 'sent'}}, function (err, done) {
                                    if (!done) {
                                        ErrorObj.addError('content update', 'Error updating status to sent');
                                        ErrorObj.errorCode = 'InvalidInput';
                                        return callback(ErrorObj);
                                    }
                                    callback();
                                });
                            }, function (e) {
                                if (!e) {
                                    callback();
                                }
                                        // Mandrill returns the error as an object with name and message keys
                            //console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
                            });
                        //maybe callback() here??
                               //console.log(email);
                        });
                    });
                });
            });
        }],
        function (err) {
            if (err) {
                return callback(err);
            }
            return callback(err, {data: 'it worked'});
        });
};
module.exports = EmailDigest;

