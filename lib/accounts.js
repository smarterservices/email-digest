'use strict';
/*jshint unused: true, node: true */
/*jslint unparam: true, node: true */

/**
 * Handles all authentication related functions
 * @class authentication
 * @required async, AppUtils, ApiToken, Account, utils, fs, underscore, RedisClient
 */

module.exports = {

    /**
     * Adds an account group.
     *
     * @async
     * @method addGroup
     * @param {String} token The token that was provided in the request.
     * @param {String} account_id The account_id that was provided in the request.
     *                            This typically will come from the URI string.
     * @param {Object} [opts] Object that holds all the api token data.
     * @param {Function} callback A function for the callback accepting the following argument 'err, ApiToken'.
     * @example
     *    function(err, ApiToken){}
     */

    addGroup: function (opts, callback) {

        console.log(opts);

        return callback(null, {});
    }

};