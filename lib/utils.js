'use strict';
/**
 * Offers several utility functions that can be used throughout the application.
 * @class utils
 * @required async, querystring, pagination, moment, node-uuid, constants
 */

var async = require('async'),
    querystring = require('querystring'),
    pagination = require('pagination'),
    moment = require('moment'),
    uuid = require('node-uuid'),
    constants = require('../config/constants.js');

module.exports = {
    Response: function () {
        var r = {},
            httpCode = 200,
            headers = [],
            content_type = 'application/json';
        r.error = false;
        r.errorCode = '';
        r.errors = [];
        r.request_id = null;
        r.ApiToken = null;
        r.account_id = null;
        r.Paging = null;

        r.getHeaders = function () {
            return headers;
        };
        r.addHeader = function (key, value) {
            headers.push({key: key, value: value});
            return this;
        };

        r.getData = function () {
            return this.data;
        };
        r.setData = function (reponseData) {
            this.data = reponseData;
        };

        r.getContentType = function () {
            return content_type;
        };
        r.setContentType = function (data) {
            content_type = data;
            return this;
        };

        r.getHttpCode = function () {
            return httpCode;
        };
        r.setHttpCode = function (data) {
            httpCode = data;
            return this;
        };

        r.handleResponse = function (Result, data, cb) {
            if (Result && (Result.hasErrors() || (Result.errorCode && Result.errorCode.length))) {
                this.errors = Result.getErrors();
                this.errorCode = Result.errorCode;
                this.error = true;
            } else {
                this.data = data;
            }
            return cb(this);
        };

        return r;
    },

    Error: function () {
        var r = {};

        r.errorCode = null;
        r.errors = [];
        r.debug_info = null;
        r.message = null;

        r.setFromEnforce = function (data) {
            if (data) {
                r.errors = r.errors.concat(data); // Merges both arrays
            }

        };

        r.getErrors = function () {

            return r.errors;
        };

        r.addError = function (property, message, value) {
            var error = {
                property: property,
                value: value,
                msg: message
            };

            r.errors.push(error);
        };

        r.hasErrors = function () {
            if (r.errors.length > 0) {
                return true;
            }
            return false;
        };

        r.addFromValidator = function (validation_result) {
            var key,
                prop_results = '',
                temp = [],
                i = 0,
                assert = '';
            if (validation_result !== true) {
                for (key in validation_result) {
                    if (validation_result.hasOwnProperty(key)) {

                        prop_results = validation_result[key];

                        if (Object.prototype.toString.call(prop_results) !== '[object Array]') {
                            temp.push(prop_results);
                            prop_results = temp;
                        }

                        for (i = 0; i < prop_results.length; i++) {
                            assert = prop_results[i].assert;
                            this.addError(key, assert.message);
                            this.errorCode = 'InvalidInput';
                        }
                    }
                }
                return true;
            }
            return false;
        };

        return r;
    },

    Paging: function (req) {
        var r = {},
            local = {pathname: ''},
            default_page_size = 50;

        r.page = 1;
        r.num_pages = 1;
        r.page_size = default_page_size;
        r.total = 0;

        r.start = 0;
        r.end = r.page_size;
        r.uri = null;
        r.first_page_uri = null;
        r.previous_page_uri = null;
        r.next_page_uri = null;
        r.last_page_uri = null;


        // handle the initial values...
        if (req) {
            if (req._parsedUrl && req._parsedUrl.pathname) {
                local.pathname = req._parsedUrl.pathname;
            } else {
                local.pathname = '/';
            }
            local.query = req.query;
            local.query_clean = req.query;

            if (req.query.page && req.query.page > 0) {
                r.page = parseInt(req.query.page, 10);
            }
            if (req.query.page_size && req.query.page_size > 0) {
                r.page_size = parseInt(req.query.page_size, 10);
            }
        } else {
            local.query_clean = {};
        }

        if (r.page > 1) {
            local.query_clean.page = r.page;
        } else {
            delete local.query_clean.page;
        }

        if (r.page_size !== default_page_size) {
            local.query_clean.page_size = r.page_size;
        } else {
            delete local.query_clean.page_size;
        }


        // calculate the start and end...
        if (r.page > 1) {
            r.start = (r.page - 1) * r.page_size;
            r.end = r.start + r.page_size - 1;
            r.uri = local.pathname + '?' + querystring.stringify(local.query_clean);
        } else {
            r.start = 0;
            r.end = r.start + r.page_size - 1;
            r.uri = local.pathname;
        }

        // set limit and skip which are used for CB paging...
        r.skip = r.start; // the index to start at...
        r.limit = r.page_size; // the total number to display...

        r.setTotal = function (count) {
            r.total = count;

            var paginator = new pagination.SearchPaginator({
                prelink: local.pathname,
                current: r.page,
                rowsPerPage: r.page_size,
                totalResult: count
            }),
                pagination_data = paginator.getPaginationData();

            r.num_pages = pagination_data.pageCount;
            r.start = pagination_data.fromResult;
            r.end = pagination_data.toResult;

            // set the last page uri
            if (r.total > 0) {
                // set the first page uri
                local.query_clean.page = 1;
                if (r.page_size !== default_page_size) {
                    local.query_clean.page_size = r.page_size;
                }

                r.first_page_uri = local.pathname + '?' + querystring.stringify(local.query_clean);

                if (r.num_pages > 1) {
                    local.query_clean.page = r.num_pages;
                    if (r.page_size !== default_page_size) {
                        local.query_clean.page_size = r.page_size;
                    }
                    r.last_page_uri = local.pathname + '?' + querystring.stringify(local.query_clean);
                }

            }

            // set the next page uri
            if (r.page < r.num_pages && r.total > 0) {
                local.query_clean.page = r.page + 1;
                if (r.page_size !== default_page_size) {
                    local.query_clean.page_size = r.page_size;
                }
                r.next_page_uri = local.pathname + '?' + querystring.stringify(local.query_clean);
            }

            // set the prev page uri
            if (r.page > 1 && r.total > 0) {
                local.query_clean.page = r.page - 1;
                if (r.page_size !== default_page_size) {
                    local.query_clean.page_size = r.page_size;
                }
                r.previous_page_uri = local.pathname + '?' + querystring.stringify(local.query_clean);
            }

            return this;

        };

        return r;
    },

    ifCondition: function (condition, ifTrue, ifFalse) {
        if (condition) {
            return ifTrue;
        }

        return ifFalse;
    },

    toBoolean: function (string) {
        if (typeof string === 'boolean') {
            return string;
        }

        if (typeof string === 'string') {
            switch (string.toLowerCase()) {
            case 'true':
            case 'yes':
            case '1':
                return true;
            case 'false':
            case 'no':
            case '0':
            case null:
                return false;
            default:
                return Boolean(string);
            }
        }

        return false;

    },

    cloneObject: function (obj) {
        if (obj === null || typeof obj !== 'object') {
            return obj;
        }

        var temp = obj.constructor(), // give temp the original obj's constructor
            key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                temp[key] = this.cloneObject(obj[key]);
            }
        }

        return temp;
    },

    timeStamp: function (precision, callback) {
        if (!precision) {
            precision = 'milliseconds';
        }
        if (precision === 'milliseconds') {
            callback(moment.utc().valueOf());
        } else {
            callback(moment().unix());
        }


    },

    randomNumber: function (max) {
        return Math.floor(Math.random() * max) + 1;
    },

    newId: function (TypeId, callback) {
        var id = uuid.v4().replace(/-/g, '');

        if ((constants.ID_PREFIXES[TypeId])) {
            if (callback) {
                callback(constants.ID_PREFIXES[TypeId] + id);
            } else {
                return constants.ID_PREFIXES[TypeId] + id;
            }
        } else {
            if (callback) {
                callback(id);
            } else {
                return id;
            }
        }
    }
};
