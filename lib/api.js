'use strict';
/*jshint unused: true, node: true */
/*jslint unparam: true, node: true */
/*jslint stupid: true, node: true */

/**
 * Provides an interface into the metadata of the REST API.
 * @class api
 * @required async, untils, fs, lodash
 */

var utils = require('./utils');
var fs      = require('fs');
var _       = require('lodash');

module.exports = {
    list : function (opts, callback) {
        var ErrorObj = utils.Error(),
            path = '../config/routes.json',
            r = '';
        // verify routes are supplied correctly
        if (_.isUndefined(path) || !_.isString(path) || !fs.existsSync(path)) {
            ErrorObj.errorCode = 'InternalError';
        }

        r = fs.readFileSync(path, {encoding: 'utf-8'});

        try {
            r = JSON.parse(r);
        } catch (e) {
            ErrorObj.errorCode = 'InternalError';
        }
        if (callback) {
            return callback(ErrorObj, r);
        }
        return {err: ErrorObj, routes: r};

    }

};