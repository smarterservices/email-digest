'use strict';
var config = require('.././config/'),
    iron_mq = require('iron_mq'),
    async = require('async'),
    imq = new iron_mq.Client({token: config.iron.mq.token, project_id: config.iron.mq.project_id}),
    queue = imq.queue('courier-' + process.env.NODE_ENV + '-messages'),
    appbootstrap = {};

appbootstrap = {
    addPush: function (callback) {
        async.series([
            function (callback) {
                if (typeof config.iron.mq.push_queue_base_url !== 'string') {
                    callback('No Base URL was passed for the IronMQ push queue.  To set this open the config file and set config.iron.mq.push_queue_base_url.');
                } else {
                    queue.add_subscribers({url: config.iron.mq.push_queue_base_url + '/' + config.api_version + '/batches/send'}, function (error, body) {
                        if (error) {
                            callback('Error:' + error);
                        } else {
                            callback();
                        }
                    });
                }
            },
            function (callback) {
                queue.info(function (error, body) {
                    if (body) {
                        console.log("--------------------");
                        console.log("Subscribers base url:");
                        console.log(body.subscribers);
                        console.log("--------------------");
                        callback();
                    } else {
                        callback('Error:' + error);
                    }
                });

            }],
            function (err) {
                if (err) {
                    return callback(err, null);
                }
                return callback(err, {worked: true});
            });
    }
};
module.exports = appbootstrap;

