'use strict';

var config = require('./global');

// if you need to overwrite any items you can put them here.

// my custom mongo test deployment...
config.mongodb = {
    host : 'ds033617.mongolab.com',
    port : 33617,
    username : 'jpiepkow',
    password : '2015Testpassword',
    database : 'email_catch'
};

module.exports = config;