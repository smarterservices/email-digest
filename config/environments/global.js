'use strict';

var config = {};

// Include the constants so they can be used from the config object...
config.CONSTANTS = require('../constants');

config.api_version = 'v1';

config.api = {
    name : 'Email Digest API Server',
    port : process.env.PORT,
    use_ssl : false
};

config.mongodb = {
    host : 'ds033617.mongolab.com',
    port : 33617,
    username : 'jpiepkow',
    password : '2015Testpassword',
    database : 'email_catch'
};

config.iron = {
    mq : {
        token: 'FHLf6PA4RVjyAf3xqK5v6Z4r0hQ',
        project_id: '5548cbd532ce5a0008000071'
    }
};

config.mandrill = {
    api_key : '236pxTPBRHwIBArrcr5u_A'
    //api_key : 'EjRAiNqF1Ow4Tu0mSjl6DQ'
};

// Standard module export.
module.exports = config;