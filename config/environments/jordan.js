'use strict';

// This must remain.
var config = require('./global');

config.api = {
    name : 'Email Digest API Server',
    port : 8000,
    use_ssl : false
};
config.iron = {
    mq : {
        token: 'FHLf6PA4RVjyAf3xqK5v6Z4r0hQ',
        project_id: '5548cbd532ce5a0008000071',
        push_queue_base_url: 'http://localhost:8000'
    }
};

// This must remain.
module.exports = config;

