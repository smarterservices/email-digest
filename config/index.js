"use strict";

var env = 'development';

if (process.env.NODE_ENV) {
    env = process.env.NODE_ENV;
}

// Print the environment we are loading
console.log('Loading Environment: ' + env);

module.exports = require(__dirname + '/environments/' + env + '.js');