'use strict';

//supervisor -- index.js

/*
 NODE_ENV=jason node index.js
 */

var config = require('./config/'),
    fs = require('fs'),
    restly = require('restly'),
    async = require('async'),
    appBootstrap = require('./lib/AppBootstrapper.js');

var server_options = {};
var server_type = 'http';

if (config.api.use_ssl) {
    server_type = 'https';
    server_options = {
        // Specify the key file for the server
        key: fs.readFileSync('ssl/production/2015/api_webhooks_io.key'),
        // Specify the certificate file
        cert: fs.readFileSync('ssl/production/2015/api_webhooks_io.crt'),
        // Specify the Certificate Authority certificate
        ca: fs.readFileSync('ssl/production/2015/api_webhooks_io_ca_bundle.crt'),
        // This is where the magic happens in Node.  All previous
        // steps simply setup SSL (except the CA).  By requesting
        // the client provide a certificate, we are essentially
        // authenticating the user.
        requestCert: true,
        // If specified as "true", no unauthenticated traffic
        // will make it to the route specified.
        rejectUnauthorized: false
    };
}

// now run the series that will actually get the application started...
async.series([
    function (callback) {
        appBootstrap.addPush(function (err) {
            if (!err) {
                return callback();
            }

            console.log('There was an issue running the app bootstrapper.');
            console.log(err);
            return callback(err);
        });
    }],
    function (err) {
        // If there are no errors, we will go ahead and start the API...
        if (!err) {
            restly.init('./config/routes.json', {
                name: config.api.name,
                port: process.env.PORT,
                description: '',
                lib: 'lib/',
                outputs: 'lib/outputs/',
                docs_endpoint: '/docs',
                caching: true,
                config: config,
                server_options: server_options,
                server_type: server_type
            });
            console.log('API', 'Started on port: ' + process.env.PORT);
        } else {
            console.log('API not started, see previous errors.');
        }
    });




