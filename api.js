//supervisor -- server.js
"use strict";

/*
 NODE_ENV=jason node server.js
 */

var config = require('./config/'),
    fs = require('fs'),
    cors = require('cors'),
    restly = require('restly'),
    appBootstrap = require("./lib/AppBootstrapper.js");

var server_options = {};
appBootstrap.addpush();
var server_type = 'http';
if (config.api.use_ssl) {
    server_type = 'https';

    server_options = {
        // Specify the key file for the server
        key: fs.readFileSync('ssl/production/2015/smarterservices.com.key'),
        // Specify the certificate file
        cert: fs.readFileSync('ssl/production/2015/smarterservices.com.crt'),
        // Specify the Certificate Authority certificate
        ca: fs.readFileSync('ssl/production/2015/smarterservices.com.chain.crt'),
        // This is where the magic happens in Node.  All previous
        // steps simply setup SSL (except the CA).  By requesting
        // the client provide a certificate, we are essentially
        // authenticating the user.
        requestCert: true,
        // If specified as "true", no unauthenticated traffic
        // will make it to the route specified.
        rejectUnauthorized: false
    };

}


// go!
restly.use(cors());
restly.init('./config/routes.json', {
    name: config.api.name,
    port: config.api.port,
    description: '',
    lib: 'lib/',
    outputs: 'lib/outputs/',
    docs_endpoint: '/docs',
    caching: true,
    config: config,
    server_options: server_options,
    server_type: server_type
});

console.log('API', 'Started on port: ' + config.api.port);


