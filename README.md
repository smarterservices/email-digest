[![Codeship Status for smarterservices/email-digest](https://codeship.com/projects/1ce524b0-e5f9-0132-1514-320dbfc00a9f/status?branch=master)](https://codeship.com/projects/82157)
[![Code Climate](https://codeclimate.com/repos/556c78b269568019210030e2/badges/aef4be7a10f3c09fbe0c/gpa.svg)](https://codeclimate.com/repos/556c78b269568019210030e2/feed)
[![Test Coverage](https://codeclimate.com/repos/556c78b269568019210030e2/badges/aef4be7a10f3c09fbe0c/coverage.svg)](https://codeclimate.com/repos/556c78b269568019210030e2/coverage)

#Getting Started:
1.Clone git repository:

 ```git clone https://jpiepkow@bitbucket.org/smarterservices/email-digest.git```
 
 2.Install node modules for testing locally or making changes:
 
 ```npm install```
 
  3.Edit configurations for Port, Mandrill, Iron or Mango:
  
   Edit Production.js:
   
   ```Location: email_digest/config/environments/production.js```
   
   Mango:
   


    host- The host of the database

    port- Port database is running on
    
    username- Username to access database
    
    password- Password to account
    
    database- The name of the databse
    
Mandrill:

	api_key- Key to the mandrill account you will use
	
Iron.io:	 
	
	Example:  

	
	config.iron = {
    mq : {
        token: 'Irontoken',
        project_id: '5548cbd529837948720008000071',
        push_queue_base_url : 'http://courier.smarterservices.com'
    }
	};
	
.

	token- The token used to connect to iron.io
	
	project_id- The Id of the project your working with.
	
	Push_queue_base_url- The base url of the api
	
4.Create Starting Documents in mango DB:

Account:

	{
    "doc_type": "account",  --does not change
    "name": "Name of the account"
	}
	
Default subscription configuration:	

		{
    "doc_type": "default-subscription-config", --does not change
    "application_id": "name of the application", 
    "email_id": "unique id for this email subscription",
    "from_email": "senders email address",
    "name": "Name of the email",
    "template": "default template for email",
    "subject": "default subject for email",
    "frequency": "Frequency for send time in cron format" --ex 0 9 1-7 * 1 *
	}
5.Set up new group and send mail:
	
New group:

```POST``` /v1/account/:account_id/groups

{

    email_id - this is the "type" of email that is going to be sent.

    group_id - the id of the group account.

    application_id - the id of the actual application.

    subject - This a for custom override of subject in the email.

    template - The template name for override.

    frequency - Frequency of send time in Cron format.

    to - Who the email goes to(separated by ,)

    bcc - bcc(also separated by ,)

}

```
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -H "Cache-Control: no-cache" -H "Postman-Token: f5ed814f-6d9a-57bf-0c39-eea2d8f84c36" -d 'application_id=the-application-id&email_id=1234&group_id=sample-group&subject=sample-subject&template=sample-template&to=jordan%40gmail.com%2Cjohn%40gmail.com&bcc=jordan%40gmail.com%2Cjeff%40gmail.com&frequency=0+9+1-7+*+1+*' http://localhost:8000/v1/account/university/groups
```

Add content to next batch for group:

```POST``` /v1/accounts/:account_id/groups/:group_id/emails/:email_id/content

{

    application_id - the id of the actual application.

    contentID - unique identifier for this content.

    content - The content(body) of the email.

}

```
curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -H "Cache-Control: no-cache" -H "Postman-Token: 38d07a44-30d9-74e1-cc1e-090b1ce23799" -d 'application_id=the-application-id&content_id=new-uuid&content=This+will+be+the+body+of+the+email' http://localhost:8000/v1/accounts/university/groups/sample-group/emails/1234/content
```
#Unit Tests


```
// to run unit tests just run the following in terminal
gulp

```

#Code Coverage

You can review the code coverage report by opening

```
/coverage/lcov-report/index.html

```

#Code Linting
In order to lint the code you can run the following command.  There results will be returned in the terminal window.  You will just need to work through the issues until it checks out clean.

```
$ gulp jslint
```

#Generate Internal API Docs
Docs can be created for the internal code.  To do this just run the following command.  After running the command you can then view the docs in the /docs directory.

```
$ gulp docs
```

#Running REST API
To start the REST API you will just need to call the following.  By default the api will run on port 8000.

```
$ node api
```

Or to set the specific environment, just add NODE_ENV={name}

```
NODE_ENV=production node api
```



## API Docs
Once the API is running, you can view the API docs by visiting http://localhost:8000/v1/docs.  Any changes to the routes can be done in the /config/routes.json file.



#Email digest system
Catches and stores emails before they are sent in order to send all of them in bulk at specific times.

*Opts Parameters:**

All Keys are required.

{

email_id - this is the "type" of email that is going to be sent.

client_id - the id of the client account.

application_id - the id of the actual application.

content_id - the unique id that is associated with this content.

content - content of the email.

batch_id - the batch that should be sent.

}


# Doc Types

This document type should be the system default for each email type.  If there are 10 email types possible in the system we would expect to see only 10 of these documents.

```
{
	doc_type : 'default-subscription-config',
	application_id : 'the-application-id',
	email_id : 'some-unique-key-for-the-batch', // document key ex. upcoming-sessions
	name : 'Name of email',
	template : 'mandrill_template_id',
	subject : 'default-subject', // ex. Your Upcoming Sessions
	frequency : '0 9 1-7 * 1 *'
}
```

For each group that is subscribed the document type below will be created.  These will relate directly to the default-subscription-config based on the application_id and email\_id.  

```
{
	doc_type : 'group-subscription-config',
	application_id : 'the-application-id',
	email_id : 'some-unique-key',
	account_id : 'the-account-id',
	group_id : 'the-group-id',
	frequency : '0 9 1-7 * 1 *',
	subject : 'custom-subject', // if this is omitted...use the default config
	template : 'custom-template',
	to : ['address','address'],
	bcc : ['address','address']
}
```

First time that content is added based on a group-subscription-config two documents will be created email-batch and batch-content. After the first time content will be added to the existing email-batch if same send time for same group.

email-batch:

	{
    "doc_type": "email-batch",
    "batch_id": "4a5e0340-053c-11e5-a877-b10d55ad3b7a",
    "send_at": 1432819261,
    "application_id": "the-application-id",
    "email_id": "1234",
    "group_id": "sample-group",
    "account_id": "university",
    "status": "sent",
    "content_ids": [
        "1test111111231"
    ],
    "ironid": "6153911695871321306"
	}

batch-content:

	{
    "doc_type": "batch-content",
    "batch_id": "5c6debc0-0548-11e5-b193-d1842cd88fbd",
    "content_id": "realtest211123123",
    "content": "realtest211123"
	}

#API Routes (planning)

## Check Health

Returns the health of the application

```GET``` /v1/health

## List Account Groups

Returns a list of all the groups for a given account.

```GET``` /accounts/:account_id/groups

## Add Account Group

Adds a new group to an account.

```POST``` /account/:account_id/groups

## Get Account Group

Returns the details of the account group

```GET``` /accounts/:account\_id/groups/:group_id

## Remove Account Group

Removes an account group.

```DELETE``` /accounts/:account\_id/groups/:group_id

## Get Group Email

Returns the details of the email the group is subscribed to.

```GET``` /accounts/:account\_id/groups/:group\_id/emails/:email_id

## List Group Emails

Returns a listing of all the emails the group is currently subscribed to.

```GET``` /accounts/:account\_id/groups/:group_id/emails

## Add Email Content

Adds email content for a specific email.

```POST``` /accounts/:account\_id/groups/:group\_id/emails/:email_id/content

## Update Email Content

Updates the content for the specific email.

```PUT``` /accounts/:account\_id/groups/:group\_id/emails/:email\_id/content/:content_id

## Get Email Content

Gets the specific content item for the email.

```GET``` /accounts/:account\_id/groups/:group\_id/emails/:email\_id/content/:content_id

## List Email Content

Gets all the email content that is being curated for the next send for a given email.

```GET``` /accounts/:account\_id/groups/:group\_id/emails/:email_id/content

## Delete Email Content

Remove the content being curated for the next send.

```DELETE```/accounts/:account_id/groups/:group_id/emails/:email_id/batches/:batch_id/content/:content_id

## Get Group Batches

Return all the batches that are currently being curated.

```GET``` /accounts/:account\_id/groups/:group_id/batches

## Remove Batch

Actually remove at batch and all content.

```DELETE``` /accounts/:account\_id/groups/:group\_id/batches/:batch_id

## Send Batch

This is called from iron.io and triggers a batch to be sent.

```POST``` /batches/send