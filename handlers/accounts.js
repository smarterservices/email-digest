'use strict';

var utils  = require('../lib/utils.js');
var accounts = require('../lib/accounts.js');
var config = require("../config/"),
    group = require('../lib/group.js')(config);


module.exports = {
    addGroup : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        group.addGroup(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    updateGroup : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        opts.group_id = opts.url_params.group_id;
        group.updateGroup(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    getGroup : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        opts.group_id = opts.url_params.group_id;
        group.getAccountGroup(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    removeGroup : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        opts.group_id = opts.url_params.group_id;
        group.removeGroup(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    listGroup : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        group.listGroups(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    getGroupEmail : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        opts.group_id = opts.url_params.group_id;
        opts.email_id = opts.url_params.email_id;
        group.getGroupEmail(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    listContent : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        opts.group_id = opts.url_params.group_id;
        opts.batch_id = opts.url_params.batch_id;
        group.listContent(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    listGroupEmail : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        opts.group_id = opts.url_params.group_id;
        group.listGroupEmails(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    listGroupBatches : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        opts.group_id = opts.url_params.group_id;
        group.listGroupBatches(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    listEmailContent : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        opts.group_id = opts.url_params.group_id;
        opts.email_id = opts.url_params.email_id;
        group.listBatchContent(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    }
};