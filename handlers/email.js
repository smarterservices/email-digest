'use strict';

var digest = require('../lib/digest.js')(config);

module.exports = {
    send : function (opts, resObj, req, res, callback) {
        digest.send(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    add : function (opts, resObj, req, res, callback) {

        opts.group_id = opts.url_params.group_id;
        opts.email_id = opts.url_params.email_id;
        opts.account_id = opts.url_params.account_id;

        digest.addContent(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    deletecontent : function (opts, resObj, req, res, callback) {
        opts.group_id = opts.url_params.group_id;
        opts.email_id = opts.url_params.email_id;
        opts.account_id = opts.url_params.account_id;
        opts.batch_id = opts.url_params.batch_id;
        opts.content_id = opts.url_params.content_id;
        digest.removeContent(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    getcontent : function (opts, resObj, req, res, callback) {
        opts.group_id = opts.url_params.group_id;
        opts.email_id = opts.url_params.email_id;
        opts.account_id = opts.url_params.account_id;
        opts.content_id = opts.url_params.content_id;
        digest.getContent(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    updatecontent : function (opts, resObj, req, res, callback) {
        opts.group_id = opts.url_params.group_id;
        opts.email_id = opts.url_params.email_id;
        opts.account_id = opts.url_params.account_id;
        opts.content_id = opts.url_params.content_id;
        digest.updateContent(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    removeBatch : function (opts, resObj, req, res, callback) {
        opts.group_id = opts.url_params.group_id;
        opts.account_id = opts.url_params.account_id;
        opts.batch_id = opts.url_params.batch_id;
        digest.removeBatch(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    addgroup : function (opts, resObj, req, res, callback) {
        digest.addGroup(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    }
};