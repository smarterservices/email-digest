'use strict';
var utils  = require('../lib/utils.js');
var api = require('../lib/api.js'),
    config = require('../config/'),
    group = require('../lib/group.js')(config);

module.exports = {
    list : function (opts, resObj, req, res, callback) {
        api.list(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    health : function (opts, resObj, req, res, callback) {
        utils.timeStamp('seconds', function (time) {
            var data = {healthy: true, unix_time: time};
            resObj.handleResponse(null, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    listaccount : function (opts, resObj, req, res, callback) {
        opts.account_id = opts.url_params.account_id;
        group.listAccounts(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    },
    listemail : function (opts, resObj, req, res, callback) {
        group.listEmail(opts, function (Result, data) {
            resObj.handleResponse(Result, data, function (resObj) {
                return callback(resObj);
            });
        });
    }
};